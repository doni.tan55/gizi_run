﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_move : MonoBehaviour
{

    public float enemy_speed = 10f;
    public bool facingRight = true;
    public Rigidbody2D rb;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        

        rb.velocity = new Vector2(enemy_speed , rb.velocity.y);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("blok"))
        {

            Vector3 characterscale = transform.localScale;
            if (facingRight == true)
            {
               
                characterscale.x = Mathf.Abs(characterscale.x);
                enemy_speed = -Mathf.Abs(enemy_speed);


                facingRight = false;
            }
            else
            {
                characterscale.x = -Mathf.Abs(characterscale.x);
                enemy_speed = Mathf.Abs(enemy_speed);
                facingRight = true;

            }


            transform.localScale = characterscale;

        }
    }





}
