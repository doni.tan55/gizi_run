﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gamecontroller : MonoBehaviour
{
    public GameObject health1;
    public GameObject health2;
    public GameObject health3;

    public string[] jenis_makanan = new string[4];
    public bool[] jawaban = new bool[4];

    public string food_now ;
    public int count = 0;
    public int health=3;

    public Text teks_besar;
    public GameObject theplayer;

    public GameObject restart_button;

    public float deathzone;
    // Start is called before the first frame update
    void Start()
    {
        food_now = jenis_makanan[0];
        teks_besar.text = "manakah yang merupakan " + food_now + " ?"; 

        
    }

    // Update is called once per frame
    void Update()
    {
        if (count > 3)
        {
            win();
        }
        if (theplayer.transform.position.y < deathzone)
        {
            lose();
        }
    }

    public void reduce_health()
    {
        if (health < 3)
        {
            health3.gameObject.SetActive(false);
        }

        if (health < 2)
        {
            health2.gameObject.SetActive(false);
        }
        if (health < 1)
        {
            health1.gameObject.SetActive(false);
            lose();
            
        }

    }

    
    public void change_food()
    {
        count += 1;
        food_now = jenis_makanan[count];
        teks_besar.text = "manakah yang merupakan " + food_now + " ?";
    }

    public GameObject winlose;
    public player_movement player;
    public Text winlosetext;
    public int level_name;
    public controller control;

    public void win()
    {
        //win
        player.stage2.SetActive(false);
        winlose.SetActive(true);
        winlosetext.text = "KAMU MENANG";
        if(level_name+1 <= controller.level_unlock.Length)
            controller.level_unlock[level_name+1] = true;

        SaveSystem.SavePlayer(control);
    }

    public void lose()
    {
        player.stage2.SetActive(false);
        winlose.SetActive(true);
        winlosetext.text = "KAMU KALAH";
        restart_button.gameObject.SetActive(true);
    }



    public void quit()
    {
        SceneManager.LoadSceneAsync("main_menu");
    }

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
