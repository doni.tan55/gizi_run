﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bg : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {

        Vector2 topRightCorner = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        AspectRatioFitter aspect = this.GetComponent<AspectRatioFitter>();


        aspect.aspectRatio = topRightCorner.x / topRightCorner.y;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
