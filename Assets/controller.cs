﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class controller : MonoBehaviour
{

    public Button[] level_button = new Button[7];
    public static bool[] level_unlock = new bool[7];

    // Start is called before the first frame update
    void Start()
    {
        if(SaveSystem.LoadPlayer() == null)
        {
            level_unlock = new bool[] {true,false,false,false,false,false,false};
        }
        else
        {
            player_data data = SaveSystem.LoadPlayer();
            level_unlock = data.array;
        }

        for (int i = 0; i < level_button.Length; i++)
        {
            if (level_unlock[i] == true)
            {
                level_button[i].interactable = true;
            }
        }
    }



    // Update is called once per frame
    void Update()
    {

    }

    public void level1()
    {

        SceneManager.LoadScene("LEVEL_1");
    }
    public void level2()
    {

        SceneManager.LoadScene("LEVEL_2");
    }
    public void level3()
    {

        SceneManager.LoadScene("LEVEL_3");
    }
    public void level4()
    {

        SceneManager.LoadScene("LEVEL_4");
    }
    public void level5()
    {

        SceneManager.LoadScene("LEVEL_5");
    }
    public void level6()
    {

        SceneManager.LoadScene("LEVEL_6");
    }
    public void level7()
    {

        SceneManager.LoadScene("LEVEL_7");
    }




}

    