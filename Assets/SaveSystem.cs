﻿using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayer(controller player)
    {
        Debug.Log("Save");
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.fun";
        Debug.Log(path);
        FileStream stream = new FileStream(path, FileMode.Create);

        player_data data = new player_data(player);
        formatter.Serialize(stream, data);
        stream.Close();

    }

    public static player_data LoadPlayer()
    {

        string path = Application.persistentDataPath + "/player.fun";
        Debug.Log(path);
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            Debug.Log("clear" + path);
            player_data data = formatter.Deserialize(stream) as player_data;
            stream.Close();
            return data;
        }
        else
        {


            return null;
        }



    }
}
