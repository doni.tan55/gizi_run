﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class foodchoice : MonoBehaviour
{

   
    public string golongan;
    public string nama;
    public Text teks;

    public gamecontroller control;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void check_answer()
    {
        if (control.food_now == golongan)
        {
            control.change_food();
            StartCoroutine("benar");

        }
        else
            StartCoroutine("salah");
            //control.reduce_health();
    }


    public GameObject tanda_benar;
    public GameObject tanda_salah;
    IEnumerator benar()
    {
        tanda_salah.gameObject.SetActive(false);
        tanda_benar.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        tanda_benar.gameObject.SetActive(false);
    }
    IEnumerator salah()
    {
        tanda_benar.gameObject.SetActive(false);
        tanda_salah.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        tanda_salah.gameObject.SetActive(false);
    }
}
