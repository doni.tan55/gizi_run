﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class position_script : MonoBehaviour
{

    public int xplus = 0;
    public int yplus = 0;



    public Vector2 ka;
    public Vector2 kb;
    public Vector2 kira;
    public Vector2 kirb;


    public bool kananatas;
    public bool kananbawah;
    public bool kiriatas;
    public bool kiribawah;

    private Vector2 screenBounds;
    // Start is called before the first frame update
    void Start()
    {
        Vector2 positioncamera = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);

        //Debug.Log(positioncamera);


        screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        ka = new Vector2(screenBounds.x, screenBounds.y);
        kb = new Vector2(screenBounds.x, positioncamera.y - selisih(screenBounds.y, positioncamera.y));
        kira = new Vector2(positioncamera.x - selisih(screenBounds.x, positioncamera.x), screenBounds.y);
        kirb = new Vector2(positioncamera.x - selisih(screenBounds.x, positioncamera.x), positioncamera.y - selisih(screenBounds.y, positioncamera.y));


        //screenBounds.x += positioncamera.x * 1;
        //screenBounds.y += positioncamera.y *-1;
        //Debug.Log(screenBounds);
        position();
    }

    float selisih(float a, float b)
    {
        return Mathf.Sqrt(Mathf.Pow(a - b, 2));
    }


    void position()
    {
        //Debug.Log(new Vector2(xplus + screenBounds.x * xtimes, yplus + screenBounds.y * ytimes));
        if (kananatas)
        {
            this.gameObject.transform.position = new Vector2(xplus + ka.x, yplus + ka.y);
        }
        if (kananbawah)
        {
            this.gameObject.transform.position = new Vector2(xplus + kb.x, yplus + kb.y);
        }
        if (kiriatas)
        {
            this.gameObject.transform.position = new Vector2(xplus + kira.x, yplus + kira.y);
        }
        if (kiribawah)
        {
            this.gameObject.transform.position = new Vector2(xplus + kirb.x, yplus + kirb.y);
        }


        //Debug.Log(this.gameObject.transform.position.x);
        //Debug.Log(this.gameObject.transform.position.y);

        //Debug.Log("=============================================");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
