﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class background : MonoBehaviour
{
    public Vector2 ka;
    public Vector2 kb;
    public Vector2 kira;
    public Vector2 kirb;
    private Vector2 screenBounds;
    // Start is called before the first frame update
    void Start()
    {

        Vector2 positioncamera = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);

        //Debug.Log(positioncamera);


        screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        ka = new Vector2(screenBounds.x, screenBounds.y);
        kb = new Vector2(screenBounds.x, positioncamera.y - selisih(screenBounds.y, positioncamera.y));
        kira = new Vector2(positioncamera.x - selisih(screenBounds.x, positioncamera.x), screenBounds.y);
        kirb = new Vector2(positioncamera.x - selisih(screenBounds.x, positioncamera.x), positioncamera.y - selisih(screenBounds.y, positioncamera.y));

        
        AspectRatioFitter aspect = this.GetComponent<AspectRatioFitter>();
        float x_pos = selisih(ka.x, kira.x)/2;
        float y_pos = selisih(ka.y, kb.y) / 2;

        aspect.aspectRatio = x_pos/ y_pos;
    }

    float selisih(float a, float b)
    {
        return Mathf.Sqrt(Mathf.Pow(a - b, 2));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
