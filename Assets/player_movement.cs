﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class player_movement : MonoBehaviour
{


    public float playerspeed = 0f;
    public bool facingRight = true;
    public float jump_power = 350f;
    public GameObject player;
    private Rigidbody2D rb;
    public gamecontroller game;
    public GameObject washbutton;
    public bool clean;
    public int urutantombol=0;

    public Animator animator;
    public bool onground;
    public float speed = 150f;

    public bool invulnerable;
    public Renderer rend;

    public GameObject stage2;

    public GameObject[] tombol= new GameObject[5];
    public GameObject[] gambarnya = new GameObject[4];

    List<Foods>list_makanan = new List<Foods>();



    // Update is called once per frame
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }
    
    void Update()
    {
        Vector3 characterscale = transform.localScale;
        if (facingRight == true)
        {
            characterscale.x = Mathf.Abs(characterscale.x);
        }
        else
        {
            characterscale.x = -Mathf.Abs(characterscale.x);
        }

        
        transform.localScale = characterscale;


        rb.velocity = new Vector2(playerspeed * Time.deltaTime, rb.velocity.y);
        animator.SetFloat("speed", Mathf.Abs(playerspeed));
        
        if (onground == true)
        {
            onLanding();
        }
   
    }

    public void onLanding()
    {
        animator.SetBool("jumping", false);
    }

   
    public void left()
    {
        if(facingRight == true)
        {
            facingRight = false;
        }
        playerspeed = -speed;
        
    }

    public void right()
    {
        if (facingRight == false)
        {
            facingRight = true;
        }
        playerspeed = speed;
        
    }

    public void zero_move()
    {
        playerspeed = 0f;
        
    }

   public void jumps()
    {
        if (onground == true)
        {
            rb.AddForce(new Vector2(rb.velocity.x, jump_power));
            onground = false;
            animator.SetBool("jumping", true);
        }

    }

    public void wash()
    {
        clean = true;
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("ground"))
        {
            onground = true;
        }
        else if (other.gameObject.CompareTag("blok"))
        {
            onground = true;
        }
        else if (other.gameObject.CompareTag("rumah"))
        {
            if(urutantombol>3 && clean == true)
            stage2.gameObject.SetActive(true);

        }

    }

    private void OnTriggerStay2D(Collider2D trig)
    {
        if (trig.gameObject.CompareTag("wastafel"))
        {
            washbutton.gameObject.SetActive(true);
            
           
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("wastafel"))
        {
            washbutton.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D trig)
    {
        if (trig.gameObject.CompareTag("mouse") && !invulnerable)
        {
            
            rend.material.color = new Color(1f, 0f, 0f, 1f);
            invulnerable = true;
            game.health -= 1;
            game.reduce_health();
        }

        if (invulnerable)
        {
            StartCoroutine("flipflop");
            
        }


        if (trig.gameObject.CompareTag("food"))
        {
            tombol[urutantombol].GetComponent<foodchoice>().nama = trig.gameObject.GetComponent<food>().namanya;
            tombol[urutantombol].GetComponent<foodchoice>().golongan = trig.gameObject.GetComponent<food>().golongan;
            tombol[urutantombol].GetComponent<Image>().sprite = trig.gameObject.GetComponent<food>().rend.sprite;
            tombol[urutantombol].GetComponent<foodchoice>().teks.text = trig.gameObject.GetComponent<food>().namanya;

            gambarnya[urutantombol].GetComponent<Image>().sprite = trig.gameObject.GetComponent<food>().rend.sprite;

            //Foods makanan = new Foods(namanya, golongan, r);
            //list_makanan.Add(new Foods (namanya, golongan, r ));
            trig.gameObject.SetActive(false);
            

            urutantombol += 1;

        }

        

    }

    IEnumerator flipflop()
    {
        
        yield return new WaitForSeconds(3);
        rend.material.color = new Color(1f, 1f, 1f, 1f);
        invulnerable = false;
        //Debug.Log("end");
    }

}



class Foods
{

    public string nama;
    public string golongan;
    public SpriteRenderer renderer;

    public Foods(string name, string tipe, SpriteRenderer render)
    {
        
        nama = name;
        golongan = tipe;
        renderer = render;
        
    }


}